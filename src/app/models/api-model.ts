export interface User{
  email:string,
  password:string
}
export interface Lista{
  nombre:string,
  tareas:Tarea[],
  otherLists?:any,
}
export interface Tarea{
  mensaje:string,
  autor:string,
}
