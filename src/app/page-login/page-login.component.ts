import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../services/toast-service';
import { environment as env } from '../../environments/environment';
import { ApiService } from '../services/api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit {

  loginForm = new FormGroup({
    email:new FormControl('', [Validators.email, Validators.required]),//, Validators.email]),
    password:new FormControl('', [Validators.required] ),
    // check:new FormControl(false, Validators.requiredTrue )
  });
  isLoading=false;
  page='login'; // 'login', 'register'

  constructor(private toastService:ToastService, private api:ApiService, private router:Router) {
    if(env && !env.production && env.form_login){
      this.loginForm.patchValue(env.form_login);
    }
  }

  ngOnInit() {
  }

  private onSubmit(data){
    if(!this.isLoading){
      this.isLoading=true;
      switch(this.page){
        case 'login':
          this.api.login(data).then((d)=>{
            this.isLoading=false;
            this.router.navigateByUrl('/kanban');
          }).catch((e)=>{
            this.isLoading=false;
            this.toastService.show(e, {classname:'bg-danger text-light', delay:4000});
          });
          break;
        case 'register':
          this.api.register(data).then((d)=>{
            this.isLoading=false;
            this.toastService.show('Registro completado', {delay:4000});
            this.page='login';
            this.loginForm.reset();
          }).catch((e)=>{
            this.isLoading=false;
            this.toastService.show(e, {classname:'bg-danger text-light', delay:4000});
          });
          break;
      }
    }
  }

}
