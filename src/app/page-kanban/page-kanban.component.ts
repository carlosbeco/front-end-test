import { Component, OnInit, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ApiService } from '../services/api.service';
import { Lista, User } from '../models/api-model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-page-kanban',
  templateUrl: './page-kanban.component.html',
  styleUrls: ['./page-kanban.component.scss']
})
export class PageKanbanComponent implements OnInit {

  isLoading=0;
  listas:Lista[]=[];
  @ViewChild('confirmModal', {static:true}) confirmModal;
  user:User=null;

  constructor(private api:ApiService, private modalService:NgbModal){
    this.listas=this.api.getListas();
    if(!this.listas){this.listas=[];}
    console.log("Listas:", this.listas);
    this.user=this.api.getUser();
  }

  ngOnInit(){}

  addTarea(lista, text){
    console.log("ls:", lista, " tx:", text);
    this.listas[lista].tareas.push({
      mensaje:text,
      autor:this.user.email
    });
    this.api.setListas(this.listas);
  }

  removeTarea(lista_index, tarea_index){
    this.listas[lista_index].tareas.splice(tarea_index, 1);
    this.listas=this.listas.slice();
    this.api.setListas(this.listas);
  }

  addList(){
    this.listas.push({
      nombre:'',
      tareas:[],
      otherLists:[],
    });
    this.api.setListas(this.listas);
  }

  removeList(i){
    if(this.listas[i].tareas.length){
      this.modalService.open(this.confirmModal, {
        ariaLabelledBy:'modal-basic-title',
        backdropClass:'modal-help-backdrop',
        windowClass:'modal-help',
        centered:true
      }).result.then((result)=>{
        if(result==='ok'){
          this.removeListForce(i);
        }
      }).catch((e)=>{
      });
    }else{
      this.removeListForce(i);
    }
  }

  removeListForce(i){
    this.listas.splice(i,1);
    this.api.setListas(this.listas);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
    this.api.setListas(this.listas);
  }

}
