import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageKanbanComponent } from './page-kanban.component';

describe('PageKanbanComponent', () => {
  let component: PageKanbanComponent;
  let fixture: ComponentFixture<PageKanbanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageKanbanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageKanbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
