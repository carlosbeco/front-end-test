import { Component, OnInit, Input, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-template-header',
  templateUrl: './template-header.component.html',
  styleUrls: ['./template-header.component.scss']
})
export class TemplateHeaderComponent implements OnInit {

  @Input() sidenav:MatSidenav;
  @Input() isLoading:boolean;
  @Input() title:string="Prevenset";
  @Input() backTo:string=null;
  @HostListener('window:resize', ['$event'])
  onResize(event){
    //event.target.innerWidth
    if(window.innerWidth<600){
      this.sidenav.toggle(false);
    }else{
      this.sidenav.toggle(true);
    }
  }


  constructor(private api:ApiService, private router:Router) { }

  ngOnInit() {
    this.onResize({});
  }

  onSidebarToggle(e){
    e.preventDefault();
    this.sidenav.toggle();
  }

  logout(){
    this.api.logout();
    this.router.navigateByUrl('/login');
  }

}
