
import { Injectable } from '@angular/core';
import { User, Lista } from '../models/api-model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private users:User[]=[];
  private user:User=null;
  private listas:any=null;

  constructor() {
    this.users=JSON.parse(localStorage.getItem('users'));
    if(!this.users){this.users=[];}
    this.user=JSON.parse(localStorage.getItem('auth'));
    this.listas=JSON.parse(localStorage.getItem('listas'));
  }

  getUser(){
    return this.user;
  }

  isLogged(){
    return this.user!=null;
  }

  async register(data){
    return new Promise((resolve, reject)=>{
      let u = this.users.find(e=>e.email===data.email);
      if(u){
        reject("El usuario ya existe");
      }else{
        this.users.push(data);
        localStorage.setItem('users', JSON.stringify(this.users));
        resolve();
      }
    });
  }

  async login(data){
    return new Promise((resolve, reject)=>{
      let u = this.users.find(e=>e.email===data.email);
      if(u && u.password===data.password){
        this.user=u;
        localStorage.setItem('auth', JSON.stringify(this.user));
        resolve();
      }else{
        reject("Usuario o contraseña incorrectos");
      }
    });
  }

  logout(){
    this.user=null;
    localStorage.setItem('auth', null);
  }

  getListas(){
    return this.listas;
  }

  setListas(listas:Lista[]){
    this.listas=listas;
    localStorage.setItem('listas', JSON.stringify(this.listas));
  }


}
