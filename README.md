# Front End Test

## Requirements

- Nodejs

## How To Run

1. Download or clone repository
2. Download dependencies running `npm install`
3. Start the server running `npm serve`
4. Open the url on your browser http://localhost:4200/

## Hot to use

1. The app will redirect you to login
2. Click on register and register a user
3. Log in, it will redirect you to Kanban Panel
4. Now you can create and delete lists and tasks.

